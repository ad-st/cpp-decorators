#include <iostream>
#include <functional>
#include <string>

using namespace std;

//simple decorate i.e. for unified error handling
template <typename T, typename... Args>
auto decorate(T fn, Args... args) -> decltype(fn(args...))
{
    try
    {
        return fn(args...);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        throw;
    }
}

//decorating with conditional execution for return type - allows creating code before and after execution
template <typename T, typename... Args>
auto decorate2(T fn, Args... args) -> decltype(fn(args...))
{
    using return_type = decltype(fn(args...));
    if constexpr (is_void<return_type>::value)
    {
        cout << "start decorate" << endl;
        fn(args...);
        cout << "end decorate" << endl;
    }
    else
    {
        cout << "start decorate" << endl;
        auto result = fn(args...);
        //do something with result
        cout << "end decorate" << endl;
        return result;
    }
}

//simple decorate with finalizer
template <typename T, typename... Args>
auto decorate3(T fn, Args... args) -> decltype(fn(args...))
{
    cout << "start decorate" << endl;
    struct F
    {
        ~F()
        {
            try
            {
                cout << "end decorate" << endl;
            }
            catch (...)
            {
                //process exception, but do not rethrow
            }
        }
    } finalizer;
    return fn(args...);
}

int decorated_int()
{
    return 10;
}

string decorated_string()
{
    return "howdy";
}

void decorated_void(int a)
{
    cout << a << endl;
}

int main()
{
    cout << decorate(decorated_int) << endl;
    cout << decorate(decorated_string) << endl;
    //cout << decorate(decorated_void) << endl; //shouldn't compile because decorated function doesn't return value
    decorate(decorated_void, 10);
    decorate([]() {
        cout << "works with lambdas" << endl;
    });
    cout << decorate([]() -> int {
        return 10;
    }) << endl;


    cout << decorate2(decorated_int) << endl;
    cout << decorate2(decorated_string) << endl;
    //cout << decorate(decorated_void) << endl; //shouldn't compile because decorated function doesn't return value
    decorate2(decorated_void, 10);
    decorate2([]() {
        cout << "works with lambdas" << endl;
    });
    cout << decorate2([]() -> int {
        return 10;
    }) << endl;


    cout << decorate3(decorated_int) << endl;
    cout << decorate3(decorated_string) << endl;
    //cout << decorate(decorated_void) << endl; //shouldn't compile because decorated function doesn't return value
    decorate3(decorated_void, 10);
    decorate3([]() {
        cout << "works with lambdas" << endl;
    });
    cout << decorate3([]() -> int {
        return 10;
    }) << endl;
}